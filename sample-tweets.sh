#!/bin/bash

echo '' ''
echo ''How many tweets are in the sample?''

echo | grep -i '{"created_at"' $1 | wc -l #every line is one tweet, and starts with {"created_at", so looking for those lines with grep and counting them wil show us how many tweets there are in the sample.


echo '' ''
echo ''How many unique tweets are in the sample?'' 

echo | cut -d'"' $1 -f14 | sort | uniq | wc -l # using cut to only return the tweet itself, this is necessary because otherwise it would be imposible to find the unique tweets. Using the uniq option to sort out al the unique tweets, sorting the tweets is necessary for this.

echo '' ''
echo ''How many retweets are in the sample?''

echo | cut -d'"' $1 -f14 | sort | uniq | grep ^RT | wc -l #every retweet starts with RT, so looking for tweets that start with RT and counting the lines (tweets) will tell us how many retweets are in the sample.

echo '' ''
echo ''First 20 sorted tweets, no retweets:''

echo | cut -d'"' $1 -f14 | sort | uniq | grep -v ^RT | head -20 #to remove all retweets, use -v option on grep. Head -20 to only show the first 20 tweets

echo '' ''
echo '' ''